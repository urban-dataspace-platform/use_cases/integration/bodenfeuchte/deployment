# NodeRED-Deployment for use case Bodenfeuchte

This project deploys a NodeRED instance with OAuth integration and NodeRED-Projects. It also create a new Grafana dashboard.

## Deployment

1. Clone this repository.
2. Add the contents of inventory.default to the end of your inventory file.
3. Fill out needed environment variables in inventory file.
    - important: for NodeRED Projects to work, you need to set the GitLab credentials
4. Adapt GitLab URLs to the nr-bodenfeuchte project (if changed).
    - the projects have to exist already when using this deployment
5. Execute playbook by running `ansible-playbook -l localhost -i inventory.yml playbook.yml`
6. Assign IDM roles as follows.

## IDM-Integration

The playbook creates a new Keycloak client for each NodeRED instance. Each user has to be assigned Admin or Viewer role for this specific client in the IDM, otherwise they won't be able to access the NodeRED instance.

1. In Keycloak, go to _Users_ and select a user.
2. Select _Role mapping_ -> _Assign Role_.
3. Select _Filter by clients_ and select Admin or Viewer for nr-bodenfeuchte-nodered client.

Now the user can either view (Viewer) or edit and deploy (Admin) the NodeRED instance.

## Contact information

This project is developed by [Hypertegrity AG](https://www.hypertegrity.de/).

## License

This project is published under EUPL 1.2.

## Link to original repository

https://gitlab.com/urban-dataspace-platform/use_cases/integration/bodenfeuchte/deployment